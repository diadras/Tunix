﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Models;
using Tunix.Classes;
using Tunix.Extensions;
using Tunix.Models;

namespace Tunix
{
    public class DashboardModel : PageModel
    {
        public string AccessToken { get; set; }

        SpotifyWebAPI api = new SpotifyWebAPI();
        Queue<FullTrack> queue = new Queue<FullTrack>();

        public void OnGet()
        {
            AuthInfo authInfo = HttpContext.Session.GetObject<AuthInfo>("AuthInfo");
            AccessToken = authInfo.AccessToken;
        }

        public JsonResult OnGetSearch2(string SearchRequest)
        {

            api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");
            SearchItem SearchResult = api.SearchItems(SearchRequest, SearchType.Track);

            return new JsonResult(SearchResult);
            
        }

        public ActionResult OnGetQueueAdd(string Id)
        {
            api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");
            FullTrack track = api.GetTrack(Id);
            if (track.Error == null)
            {
                Queue<FullTrack> queueSession = HttpContext.Session.GetObject<Queue<FullTrack>>("queue");

                if (queueSession != null)
                {
                    queue = queueSession;
                }
                 
                queue.Enqueue(track);
                HttpContext.Session.SetObject("queue", queue);

                return new JsonResult(queue);
            }
            else
            {
                return new JsonResult(track.Error);
            }
        }
 
        public void OnGetPlayQueue(string Id)
        {
            string DeviceID = "";

            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");

            string trackuri = "spotify:track:"+Id;

            AvailabeDevices Device = api.GetDevices();

            foreach (var devices in Device.Devices)
            {
                if(devices.Name == "Tunix")
                {
                    DeviceID = devices.Id;
                    break;
                }
            }

            if (Device.Devices != null)
            {
                List<string> trackuris = new List<string> { trackuri };
                ErrorResponse error = api.ResumePlayback(DeviceID, null, trackuris, 0, 0);
                
            }
        }
    }
}