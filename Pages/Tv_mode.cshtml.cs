﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Models;
using Tunix.Classes;
using Tunix.Extensions;
using Tunix.Models;

namespace Tunix.Pages
{
    public class Tv_modeModel : PageModel
    {
        public string AccessToken { get; set; }
        Queue<FullTrack> queue = new Queue<FullTrack>();
        private int progress;

        public void OnGet()
        {
            AuthInfo authInfo = HttpContext.Session.GetObject<AuthInfo>("AuthInfo");
            AccessToken = authInfo.AccessToken;
        }

        public JsonResult OnGetQueue()
        {
            List<FullTrack> TrackList = new List<FullTrack>();
            Queue<FullTrack> TrackQueue = HttpContext.Session.GetObject<Queue<FullTrack>>("queue");

            if (TrackQueue != null)
            {
                int Counter = TrackQueue.Count();

                for (int i = 0; i < Counter; i++) { 
                    TrackList.Add(TrackQueue.Peek());
                    TrackQueue.Dequeue();
                }
            }
            return new JsonResult(TrackList);
        }

        public void OnGetPlayQueue(string Id)
        {
            string DeviceID = "";
            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");
            string trackuri = "spotify:track:" + Id;
            AvailabeDevices Device = api.GetDevices();

            foreach (var devices in Device.Devices)
            {
                if (devices.Name == "Tunix")
                {
                    DeviceID = devices.Id;
                    break;
                }
            }

            if (Device.Devices != null)
            {
                List<string> trackuris = new List<string> { trackuri };
                ErrorResponse error = api.ResumePlayback(DeviceID, null, trackuris, 0, 0);

            }
        }

        public void OnGetPause()
        {
            string DeviceID = "";
            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");
            AvailabeDevices Device = api.GetDevices();

            foreach (var devices in Device.Devices)
            {
                if (devices.Name == "Tunix")
                {
                    DeviceID = devices.Id;
                    break;
                }
            }
            PlaybackContext playback = api.GetPlayback();
            
            HttpContext.Session.SetInt32("progress", playback.ProgressMs);

            if (Device.Devices != null)
            {
                api.PausePlayback(DeviceID);
            }
        }

        public void OnGetPlay(string Id)
        {
            string DeviceID = "";
            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");
            string trackuri = "spotify:track:" + Id;
            AvailabeDevices Device = api.GetDevices();
   

            foreach (var devices in Device.Devices)
            {
                if (devices.Name == "Tunix")
                {
                    DeviceID = devices.Id;
                    break;
                }
            }

            if (Device.Devices != null)
            {
                List<string> trackuris = new List<string> { trackuri };
                string progress2 = HttpContext.Session.GetInt32("progress").ToString();
                api.ResumePlayback(DeviceID, null, trackuris, 0, int.Parse(progress2));
                 
            }
        }
    }
}