﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Tunix.Models;
using Tunix.Extensions;
using SpotifyAPI.Web.Models;
using SpotifyAPI.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Text;

namespace Tunix.Pages
{
    public class CallbackModel : PageModel
    {
        //GET de code van de response van Spotify na /SpotifyLogin
        //"code" is lowercase omdat dit de naam is van de parameter
        [BindProperty(SupportsGet = true)]
        public string code { get; set; }
        AuthInfo authInfo = new AuthInfo();
        Session session = new Session();
        public IActionResult OnGet()
        {
            string responseString = "";
            string redirectUri = "";
            //Check of de code iets bevat
            if (code.Length > 0)
            {
                Console.WriteLine("Current Host value: " + HttpContext.Request.Host.Value);
                if(HttpContext.Request.Host.Value == "www.diadras.net")
                {
                    redirectUri = "http://www.diadras.net/callback";
                } else if(HttpContext.Request.Host.Value == "diadras.net")
                {
                    redirectUri = "http://diadras.net/callback";
                } else if(HttpContext.Request.Host.Value == "localhost:52845")
                {
                    redirectUri = "http://localhost:52845/callback";
                }
                Console.WriteLine("Callback URI set to: " + redirectUri);

                //Maakt nieuw HTTP request
                using (HttpClient client = new HttpClient())
                {
                    //Geeft de client_id en client_secret mee in de header
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes("8d35b96a3f8848b19bea52857e8d1110" + ":" + "5aaa7918c43a41b8beba8ea6186c83f0")));
                    FormUrlEncodedContent formContent = new FormUrlEncodedContent(new[]
                    {
                        //Geef benodigde informatie mee voor access_token aanvraag
                        new KeyValuePair<string, string>("code", code),
                        new KeyValuePair<string, string>("redirect_uri", redirectUri),
                        new KeyValuePair<string, string>("grant_type", "authorization_code")
                    });
                    //Post request naar spotify
                    var response = client.PostAsync("https://accounts.spotify.com/api/token", formContent).Result;

                    //Lees response uit en zet in responseString als string
                    var responseContent = response.Content;
                    responseString = responseContent.ReadAsStringAsync().Result;

                    JObject jObject = JObject.Parse(responseString);
                    //authInfo = new AuthInfo
                    //{
                    if(jObject["access_token"] != null) { 
                        authInfo.AccessToken = (string)jObject["access_token"]; 
                    } else
                    {
                        authInfo.AccessToken = "empty access_token";
                    }

                    if (jObject["token_type"] != null)
                    {
                        authInfo.TokenType = (string)jObject["token_type"];
                    }
                    else
                    {
                        authInfo.TokenType = "empty token_type";
                    }

                    if (jObject["expires_in"] != null)
                    {
                        authInfo.ExpiresIn = (int)jObject["expires_in"];
                    }
                    else
                    {
                        authInfo.ExpiresIn = 0;
                    }

                    if (jObject["refresh_token"] != null)
                    {
                        authInfo.RefreshToken = (string)jObject["refresh_token"];
                    }
                    else
                    {
                        authInfo.RefreshToken = "empty refresh_token";
                    }
                    if (jObject["scope"] != null)
                    {
                        authInfo.Scope = (string)jObject["scope"];
                    }
                    else
                    {
                        authInfo.Scope = "empty refresh_token";
                    }
                    Console.WriteLine("authInfo.AccessToken: " + authInfo.AccessToken);
                    Console.WriteLine("authInfo.TokenType: " + authInfo.TokenType);
                    Console.WriteLine("authInfo.ExpiresIn: " + authInfo.ExpiresIn);
                    Console.WriteLine("authInfo.RefreshToken: " + authInfo.RefreshToken);
                    Console.WriteLine("authInfo.Scope: " + authInfo.Scope);
                    HttpContext.Session.SetObject("AuthInfo", authInfo);

                    //};

                }

                //Vul AuthInfo object met informatie uit de response JSON string
                /*JObject jObject = JObject.Parse(responseString);
                AuthInfo authInfo = new AuthInfo
                {
                    AccessToken = (string)jObject["access_token"],
                    TokenType = (string)jObject["token_type"],
                    ExpiresIn = (int)jObject["expires_in"],
                    RefreshToken = (string)jObject["refresh_token"],
                    Scope = (string)jObject["scope"]
                };*/

                //Geef object mee naar sessie onder naam/key AuthInfo
                
            }

            SpotifyWebAPI api = new SpotifyWebAPI()
            {
                AccessToken = authInfo.AccessToken,
                TokenType = authInfo.TokenType,
                // Wanneer UseAuth niet wordt gebruikt, gebruik je niet de authorisatie van de user (aka access_token)
                UseAuth = true
            };
            HttpContext.Session.SetObject("api", api);
            
            return RedirectToPage("Dashboard");            
        }
    }
}