﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Models;
using SpotifyAPI.Web.Enums;
using Tunix.Extensions;
using Tunix.Models;

//Het is niet mogelijk om op een voor mij toegankelijke manier een POST request aan te maken naar Spotify.
//Vandaar dat er een authentication scheme wordt gebruikt. [Authorize] maakt de pagina onbruikbaar voor andere code behalve de auth
//Na het verifieren wordt ge-redirect van Spotify naar /callback

namespace Tunix.Pages
{
    [Authorize(AuthenticationSchemes = "Spotify")]
    public class SpotifyLoginModel : PageModel
    {
        //Niks in deze class wordt gebruikt, dit komt door [Authorize(AuthenticationSchemes = "Spotify")] die de pagina compleet afvangt

        /*private readonly ILogger<SpotifyLoginModel> _logger;

        public SpotifyLoginModel(ILogger<SpotifyLoginModel> logger)
        {
            _logger = logger;
        }

        public async void OnGetAsync()
        {
            await HttpContext.GetTokenAsync("Spotify", null);
        }*/
    }
}