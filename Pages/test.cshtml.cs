﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Tunix.Models;
using Microsoft.AspNetCore.Http;
using Tunix.Classes;
using SpotifyAPI.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Tunix.Extensions;
using SpotifyAPI.Web.Models;

namespace Tunix.Pages
{
    
    public class TestModel : PageModel
    {
        private readonly ILogger<TestModel> _logger;
        public string Result { get; set; }
        public string Query { get; }
        public List<Account> outputList { get; set; }

        public SpotifyWebAPI api { get; set; }

        public string AccessToken { get; set; }
        public string code { get; set; } = "Not set yet";

        private Authenticate auth = new Authenticate();

        [BindProperty]
        public string Username { get; set; }
        [BindProperty]
        public string Password { get; set; }

        public TestModel(ILogger<TestModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            outputList = auth.GetAccount();
            //code = HttpContext.Session.GetString("AccessToken");
        }

        public IActionResult OnPostRequest()
        {
            return RedirectToPage("SpotifyRequest");
        }

        public IActionResult OnPostGenerate()
        {
            outputList = auth.GetAccount();

            AuthInfo authInfo = HttpContext.Session.GetObject<AuthInfo>("AuthInfo");
            code = authInfo.AccessToken;
            return Page();
        }

        public IActionResult OnPostLogin()
        {
            if (Username!= null && Password != null)
            {
                HttpContext.Session.SetString("username", Username);
                HttpContext.Session.SetString("password", Password);
                Result += "Login valid";
                return Page();
            }
            else
            {
                Result += "Login invalid ";
                return Page();
            }
        }

        public IActionResult OnPostRegister()
        {
            if (Username != null && Password != null)
            {
                HttpContext.Session.SetString("username", Username);
                HttpContext.Session.SetString("password", Password);

                if (Username != null && Password != null)
                {
                    if (auth.SetAccount(Username, Password))
                    {
                        Result = "Register successful";
                    }
                    else
                    {
                        Result = "Register unsuccessful";
                    }
                }
                else
                {
                    Result = "Username and/or password empty";
                }
                return Page();
            }
            else
            {
                Result += "Login invalid ";
                return Page();
            }
        }
    }
}
