﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Models;
using SpotifyAPI.Web.Enums;
using Tunix.Extensions;
using Tunix.Models;

namespace Tunix
{
    public class SpotifyRequestModel : PageModel
    {
        public FullTrack Track { get; set; }
        public PrivateProfile Profile { get; set; }
        public SearchItem SearchResult { get; set; }
        public string AccessToken { get; set; }
        [BindProperty]
        public string SearchRequest { get; set; }
        
        public void OnGet()
        {
            AuthInfo authInfo = HttpContext.Session.GetObject<AuthInfo>("AuthInfo");
            AccessToken = authInfo.AccessToken;
        }

        public async Task<IActionResult> OnPostAccount()
        {
            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");

            AvailabeDevices Device = await api.GetDevicesAsync();
            if (Device.Devices != null)
            {
                
            }

            PlaybackContext Playback = await api.GetPlayingTrackAsync();
            Track = Playback.Item;

            Profile = await api.GetPrivateProfileAsync();
            if (!Profile.HasError())
            {
                
            }

            return Page();
        }

        public async void OnPostPause()
        {
            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");

            AvailabeDevices Device = await api.GetDevicesAsync();
            if (Device.Devices != null)
            {
                //HttpContext.Session.SetString("DevID", device.Devices[0].Id.ToString());
                ErrorResponse error = await api.PausePlaybackAsync(Device.Devices[0].Id.ToString());
            }
        }

        public IActionResult OnPostTriggerPlayback()
        {
            AuthInfo authInfo = HttpContext.Session.GetObject<AuthInfo>("AuthInfo");
            AccessToken = authInfo.AccessToken;
            return Page();
        }

        public void OnPostPlay()
        {
            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");
            PlaybackContext test = api.GetPlayingTrack();
            string contextUri = test.Context.Uri;

            AvailabeDevices Device = api.GetDevices();
            if (Device.Devices != null)
            {
               //api.ResumePlayback(Device.Devices[0].ToString(),contextUri);
            }
        }

        public async Task<IActionResult> OnPostSearch()
        {
            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");
            
            SearchResult = await api.SearchItemsAsync(SearchRequest, SearchType.Track);
            return Page();
        }

        public JsonResult OnPostSearch2()
        {
            SpotifyWebAPI api = HttpContext.Session.GetObject<SpotifyWebAPI>("api");

            SearchResult = api.SearchItems(SearchRequest, SearchType.Track);

            return new JsonResult(SearchResult);
        }
    }
}