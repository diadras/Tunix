﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Models;

namespace Tunix.Models
{
    public class Session
    {
        public SpotifyWebAPI api { get; set; }
        public AuthInfo authInfo { get; set; }
        public Queue<FullTrack> queue { get; set; }
        public string sessionKey { get; set; }

    }
}
