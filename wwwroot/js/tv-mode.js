﻿function PlayPause() {
    var Id = $('.pause').attr('id');
    var State = $('.pause').attr('data');

    console.log(State);

    if (State == "0") {
        $.ajax({
            type: "GET",
            url: "Tv_mode?handler=Pause",
            contentType: "plain/text",
            dataType: "text",
            data: { 'Id': Id },
            success: function () {
                $('.pause').attr('data', "1");
                $('.pause').attr('src', "/img/play.svg");
            },
            failure: function (response) {
                alert(response);
            }
        });
    } else {
        $.ajax({
            type: "GET",
            url: "Tv_mode?handler=Play",
            contentType: "plain/text",
            dataType: "text",
            data: { 'Id': Id },
            success: function () {
                $('.pause').attr('data', "0");
                $('.pause').attr('src', "/img/pause.png");
            },
            failure: function (response) {
                alert(response);
            }
        });
    }
}