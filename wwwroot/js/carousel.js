

$(document).ready(function () {


    var imageCover = $(".cover img");
    var source = imageCover.attr("src");

    $(".playlist li a").hover(function () {
        $(imageCover).addClass("transition");
        var hoverItem = $(this).addClass("hover");
        imageCover.attr("src", hoverItem.data("image"));
    }, function () {
        $(imageCover).removeClass("transition");
        var hoverItem = $(this).remove("hover");
        imageCover.attr("src", source);
    });

    $(".arrowDown").click(function () {
        $('.backgroundWrapper').addClass('hide');
        $('.extraTitle').addClass('test');
        $('.first').addClass('test');
        $('.second').addClass('test');
        $('.third').addClass('test');
    });

    $('html').on('mousewheel', function (e) {
        $('.backgroundWrapper').addClass('hide');
    });

    function moveToSelected(element) {

        if (element == "nextAlbum") {
            var selected = $(".selected").next();
            console.log(selected);
        } else if (element == "prevAlbum") {
            var selected = $(".selected").prev();
        } else {
            var selected = element;
        }

        var next = $(selected).next();
        var prev = $(selected).prev();
        var prevSecond = $(prev).prev();
        var nextSecond = $(next).next();

        $(selected).removeClass().last().addClass("slideOuter selected");

        $(prev).removeClass().addClass("slideOuter prev");
        $(next).removeClass().addClass("slideOuter next");

        $(nextSecond).removeClass().addClass("slideOuter nextSecond");
        $(prevSecond).removeClass().addClass("slideOuter prevSecond");

        $(nextSecond).nextAll().removeClass().addClass('slideOuter nextHide');
        $(prevSecond).prevAll().removeClass().addClass('slideOuter prevHide');

    }


    $('.consoleNext').click(function () {
        moveToSelected('nextAlbum');
    });

    $('.consolePrev').click(function () {
        moveToSelected('prevAlbum');
    });

});