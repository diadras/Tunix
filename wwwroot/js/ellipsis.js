﻿$(function() {
    $(".content").on("mouseover", function() {

        var element = $(this)
                .clone().children()
                .css({display: 'inline', width: 'auto', visibility: 'hidden'})
                .appendTo('body');

        if ((element.width() + 28) > $(this).width()) {
            $(this).children().addClass( "autoscroll" );
        }
    });

    $(".content").on("mouseleave", function () {
        $(this).children().removeClass( "autoscroll" );
    });
});