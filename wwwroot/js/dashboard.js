﻿function QueueSong(id) {

    var songId = id;

    $.ajax({
        type: "GET",
        url: "Dashboard?handler=QueueAdd",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { 'Id': songId },
        success: function (response) {
            $(".playlist").empty();
            for (var i = 0; i < response.length; i++) {
                $(".playlist").append("<li class='col-12 afterAppend'><a class='afterAppendImg' data-image='" + response[i].album.images[1].url + "'>"+ response[i].name +"</a><a>" + response[i].album.artists[0].name + "<span> &#183; </span>" + response[i].album.name +"</a></li>");
            }
        },
        failure: function (response) {
            alert(response);
        }
    });

    $.ajax({
        type: "GET",
        url: "Dashboard?handler=PlayQueue",
        contentType: "plain/text",
        dataType: "text",
        data: { 'Id': songId },
        success: function () {
        },
        failure: function (response) {
            alert(response);
        }
    });
}

function moveToSelected(element) {

    if (element == "nextAlbum") {
        var selected = $(".selected").next();
        console.log(selected);
    } else if (element == "prevAlbum") {
        var selected = $(".selected").prev();
    } else {
        var selected = element;
    }

    var next = $(selected).next();
    var prev = $(selected).prev();
    var prevSecond = $(prev).prev();
    var nextSecond = $(next).next();

    $(selected).removeClass().last().addClass("slideOuter selected");

    $(prev).removeClass().addClass("slideOuter prev");
    $(next).removeClass().addClass("slideOuter next");

    $(nextSecond).removeClass().addClass("slideOuter nextSecond");
    $(prevSecond).removeClass().addClass("slideOuter prevSecond");

    $(nextSecond).nextAll().removeClass().addClass('slideOuter nextHide');
    $(prevSecond).prevAll().removeClass().addClass('slideOuter prevHide');

}

$('.fa-chevron-right').click(function () {
    moveToSelected('nextAlbum');
});

$('.fa-chevron-left').click(function () {
    moveToSelected('prevAlbum');
});

var imageCover = $(".coverInner img");
var source = imageCover.attr("src");

$(".playlist").on("mouseenter", ".afterAppend", function () {
    $(imageCover).addClass("transition");
    var hoverItem = $(this).find(".afterAppendImg").addClass("hover");
    imageCover.attr("src", hoverItem.data("image"));
});

$(".playlist").on("mouseleave", ".afterAppend", function () {
    $(imageCover).removeClass("transition");
    var hoverItem = $(this).find(".afterAppendImg").removeClass("hover");
    imageCover.attr("src", source);
});
