﻿$("#search").keyup(function (event) {

    var SearchRequest = $(this).val();

    $.ajax({
        type: "GET",
        url: "Dashboard?handler=Search2",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { 'SearchRequest': SearchRequest },
        success: function (response) {
            $(".searchResult").empty();
            console.log(response);

            var tracks = response.tracks;
            var items = tracks.items
            for (var i = 0; i < items.length; i++) {
                if (items[i].album.artists[1] != null) {
                    var feature = ' ft. ' + items[i].album.artists[1].name;
                } else {
                    var feature = "";
                }

                $(".searchResult").append("<div class='row searchSong'><div class='col col-2 songCover'><img src='" + items[i].album.images[1].url + "'></div><div class='col-8 songInfo'><div class='row'><div class='col-12 songTitle'><span>" + items[i].name + "</span></div><div class='col-12 songArtist'><span>" + items[i].album.artists[0].name + feature + "</span></div></div></div><div class='col-2 addSong'><i onclick='QueueSong(\"" + items[i].id + "\")' class='fas fa-plus'></i></div></div>");
            }
        },
        failure: function (response) {
            alert(response);
        }
    });

});