using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Enums;

namespace Tunix
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            services.AddRazorPages();
            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
                options.IdleTimeout = TimeSpan.FromSeconds(1800);
            });
            services.AddAuthentication(o => o.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie()
                .AddSpotify(options =>
                {
                    var scopes = Scope.UserLibraryRead;
                    options.Scope.Add(scopes.GetStringAttribute(","));

                    options.SaveTokens = true;
                    /*options.ClientId = "8d35b96a3f8848b19bea52857e8d1110";
                    options.ClientSecret = "b761469a94e04554abc50435c765927b";
                    options.CallbackPath = "/callback";*/
                    options.ClientId = Configuration["Spotify:ClientID"];
                    options.ClientSecret = Configuration["Spotify:ClientSecret"];
                    options.CallbackPath = "/callback";
                    options.Scope.Add("user-read-playback-state");
                    options.Scope.Add("user-modify-playback-state");
                    options.Scope.Add("user-read-currently-playing");
                    options.Scope.Add("streaming");
                    options.Scope.Add("user-read-email");
                    options.Scope.Add("user-read-private");

                });
            services.AddMvc(option => option.EnableEndpointRouting = false);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
