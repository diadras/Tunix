﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tunix.Models;
using MySql.Data;
using MySql.Data.MySqlClient;



namespace Tunix.Classes
{
    public class Authenticate
    {
        private string connStr = "server=diadras.net;user id=proftaakuser;password=wachtwoord;persistsecurityinfo=True;database=proftaak";
        public string Result { get; set; }
        public bool Check { get; set; }
        public List<Account> outputList { get; set; }

        public List<Account> GetAccount(string query = null)
        {
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                if (query == null)
                {
                    query = "SELECT id, username, password FROM accounts;";
                }
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();

                outputList = new List<Account>();

                while (reader.Read())
                {
                    outputList.Add(new Account
                    {
                        id = Convert.ToInt32(reader[0]),
                        username = reader[1].ToString(),
                        password = reader[2].ToString()
                    });
                }

                reader.Close();
            }

            catch (Exception ex)
            {
                Exception argEx = new Exception("Error getting data from server", ex);
                throw argEx;
            }

            finally
            {
                conn.Close();
            }
            return outputList;
        }

        public bool SetAccount(string username, string password)
        {
            bool Created;
            MySqlConnection conn = new MySqlConnection(connStr);
            
            string query = $"INSERT INTO accounts (username, password) VALUES ('{username}','{password}');";
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                if (cmd.ExecuteNonQuery() != -1)
                {
                    Created = true;
                }
                else
                {
                    Created = false;
                }

            }
            catch (Exception ex)
            {
                Exception argEx = new Exception("Error inserting data to server", ex);
                throw argEx;
            }
            finally
            {
                conn.Close();
            }
            return Created;
            
        }

        public bool CheckAccount(string username, string password)
        {
            bool Check = false;
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                string query = $"SELECT password FROM accounts WHERE username='{username}';";

                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataReader reader = cmd.ExecuteReader();

                outputList = new List<Account>();

                while (reader.Read())
                {
                    if (reader[0].ToString() == password)
                    {
                        Check = true;
                        break;
                    } else
                    {
                        Check = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Exception argEx = new Exception("Error getting data from server", ex);
                throw argEx;
            }
            finally
            {
                conn.Close();
            }
            return Check;
        }
    }
}
